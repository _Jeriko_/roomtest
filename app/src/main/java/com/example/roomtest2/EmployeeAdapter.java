package com.example.roomtest2;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

public class EmployeeAdapter extends RecyclerView.Adapter<EmployeeHolder> {
    private List<Employee> list;
    private Context context;
    public EmployeeAdapter(List<Employee> list, Context context){
        this.list = list;
        this.context = context;
    }
    @NonNull
    @Override
    public EmployeeHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = LayoutInflater.from(context);
        return new EmployeeHolder(inflater,viewGroup);
    }

    @Override
    public void onBindViewHolder(@NonNull EmployeeHolder employeeHolder, int i) {
        employeeHolder.bind(list.get(i));
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
