package com.example.roomtest2;

import android.arch.persistence.room.Room;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;

import java.util.List;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
        public void addEmployee(View view){
        EditText employeeId = findViewById(R.id.employee_id_input_field);
        EditText employeeName = findViewById(R.id.employee_name_input_field);
        EditText employeeSalary = findViewById(R.id.employee_salary_input_field);

        Employee employee = new Employee();
        employee.setId(Long.valueOf(employeeId.getText().toString()));
        employee.setName(employeeName.getText().toString());
        employee.setSalary(Integer.valueOf(employeeSalary.getText().toString()));

        new Thread(()->{
            AppDatabase db =  App.getInstance().getDatabase();
            EmployeeDao employeeDao = db.employeeDao();

            employeeDao.insert(employee);
            Log.i("Database","Employee added");
        }).start();

        employeeId.setText("");
        employeeName.setText("");
        employeeSalary.setText("");
    }
    public void viewEmployees(View view){

        updateUI(App.getInstance().getDatabase().employeeDao().getAll());
    }
    public void addNewEmployee(View view){
        FrameLayout frameLayout = findViewById(R.id.view_employees_layout);
        frameLayout.setVisibility(View.GONE);

        LinearLayout layout = findViewById(R.id.add_employee_layout);
        layout.setVisibility(View.VISIBLE);
    }
    public void updateUI(List<Employee> list){
        LinearLayout layout = findViewById(R.id.add_employee_layout);
        layout.setVisibility(View.GONE);

        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        FrameLayout frameLayout = findViewById(R.id.view_employees_layout);
        frameLayout.setVisibility(View.VISIBLE);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setAdapter(new EmployeeAdapter(list,this));
    }
}
