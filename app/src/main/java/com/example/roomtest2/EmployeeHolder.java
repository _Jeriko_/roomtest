package com.example.roomtest2;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.TextView;

public class EmployeeHolder extends RecyclerView.ViewHolder {
    private TextView employeeId;
    private TextView employeeName;
    private TextView employeeSalary;
    public EmployeeHolder(LayoutInflater inflater, ViewGroup container){
        super(inflater.inflate(R.layout.employee,container,false));

        employeeId = itemView.findViewById(R.id.employee_id);
        employeeName = itemView.findViewById(R.id.employee_name);
        employeeSalary = itemView.findViewById(R.id.employee_salary);
    }
    public void bind(Employee employee){
        employeeId.setText(employeeId.getText() + " : " + String.valueOf(employee.getId()));
        employeeName.setText(employeeName.getText() + " : " + employee.getName());
        employeeSalary.setText(employeeSalary.getText() + " : " + String.valueOf(employee.getSalary()));
    }
}
